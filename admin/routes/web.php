<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', function () {
    return view('auth.login');
})->name("login");
Route::get("logout", function(){
  Auth::logout();
  return redirect('/');
});
Route::post("login", "UserController@login");

Route::group(['middleware' => 'auth'], function(){
  Route::get('/', 'DashboardController@view');

  //packages routes
  Route::get("quizes", "QuizController@viewAll");
  Route::post("quizes", "QuizController@store");
  Route::get("quizes/{id}", "QuizController@getQuiz");
  Route::put("quizes/{id}", "QuizController@setQuiz");
  Route::post("quizes/{id}/{status}", "QuizController@toggleStatus");

    Route::get("user-answers", "UserAnswersController@viewAll");
    Route::post("user-answers", "UserAnswersController@getAnswersData");
});
