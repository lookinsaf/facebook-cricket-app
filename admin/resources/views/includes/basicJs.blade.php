<!-- jQuery 3 -->
<script src="{{url("/assets/jquery/dist/jquery.min.js")}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{url("/assets/jquery-ui/jquery-ui.min.js")}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{url("/assets/bootstrap/js/bootstrap.min.js")}}"></script>
{{-- select2 --}}
{{-- <script src="{{url('assets/select2/js/select2.full.min.js')}}"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js"></script>
<script type="text/javascript">
$(".select2").select2({
    tags: true,
    dropdownParent: $("#addModal"),
    allowClear: true
});
</script>
<script src="{{url('assets/js/ajax_request_1.1.js')}}"></script>


<script src="{{url('assets/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('assets/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

<!-- AdminLTE App -->
<script src="{{url('assets/js/adminlte.min.js')}}"></script>
