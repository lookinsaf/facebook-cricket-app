<html>
    <head>
      <meta name="csrf-token" content="{{ csrf_token() }}" />
      <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>@yield('title') |  Facebook cricket quizz</title>
        <!-- Tell the browser to be responsive to screen width -->
 <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
 @include('includes.basicCss')
 @yield('aditionalCss')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
      @include('includes.mainHeader')
      @include('includes.sideBar')

        <div class="content-wrapper">
            @yield('content')
        </div>
        <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>
   @include('includes.basicJs')
   @yield('aditionalJs')
    </body>
</html>
