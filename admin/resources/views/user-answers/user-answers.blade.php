@extends('includes.app')
@section('title', 'User Answers')
@section('aditionalCss')
  <style>
    table.dataTable tbody td {
      word-break: break-word;
      vertical-align: top;
    }
  </style>
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.4.1/css/buttons.dataTables.min.css">
  @endsection
@section('content')
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Answers
        <small>All the User Answers</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
    <div class="content">

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <div class="col-xs-12">
                <div class="form-group col-md-5">
                  <h5>Start Date <span class="text-danger"></span></h5>
                  <div class="controls">
                    <input type="date" name="start_date" id="start_date" class="form-control datepicker-autoclose" placeholder="Please select start date"> <div class="help-block"></div></div>
                </div>
                <div class="form-group col-md-5">
                  <h5>End Date <span class="text-danger"></span></h5>
                  <div class="controls">
                    <input type="date" name="end_date" id="end_date" class="form-control datepicker-autoclose" placeholder="Please select end date"> <div class="help-block"></div></div>
                </div>
                <div class="form-group col-md-2" >
                  <h5> <span class="text-danger">
                      <br>
                    </span></h5>
                  <button type="text" id="btnFiterSubmitSearch" class="btn btn-info">Filter</button>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive ">
        <table class="table table-bordered table-hover table-responsive" width="100%">
          <thead>
            <tr>
              <th >User Name</th>
              <th>Email</th>
              <th>Score</th>
              <th>Date</th>
            </tr>
          </thead>
        </table>
</div>
</div>
      </div>
    </div>
    </div>

@endsection

@section('aditionalJs')
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.4.1/js/dataTables.buttons.min.js"></script>
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.4.1/js/buttons.flash.min.js"></script>
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.4.1/js/buttons.html5.min.js"></script>
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.4.1/js/buttons.print.min.js"></script>
  <script type="text/javascript">
  // start use for request js
    baseUrl =  "{{url("user-answers")}}";
      url =  baseUrl;
      primaryKey = "id"; // use to set update url
      reloadAfterSubmit = true;
      // end use for request js

      function showForm() {
        resetForm("quizeForm");
        $('#addModal').modal('show')
      }
      function submitData(formId, type) {
        var formData = new FormData(document.querySelector("#" + formId))
        formData.set("correct_answer", $('#addModal').find('input[name="correct_answer"]:checked').data('value'))
        submitForm(formData);
      }
  $('.table').DataTable({
    "lengthMenu": [[25, 50, 75, 100, 150, -1], [25, 50, 75, 100, 150, 'All']],
   "processing": true,
   "serverSide": true,
    ajax: {
      url: "{{ url('user-answers') }}",
      type: 'POST',
      data: function (d) {
        d.start_date = $('#start_date').val();
        d.end_date = $('#end_date').val();
      }
    },
   "columns": [
     { "data": "user_name" , name : "user_name"},
     { "data": "email", name : "email" },
     { "data": "score", name : "score" },
     { "data": "date", name : "date" }
   ],
   'paging'      : true,
   'lengthChange': true,
   'searching'   : true,
   'ordering'    : true,
   'info'        : true,
   'autoWidth'   : true,
   dom: 'lBfrtip',

   buttons: [
      'excel', 'csv', 'pdf', 'print'
   ]
 });
  $('#btnFiterSubmitSearch').click(function(){
    $('.table').DataTable().draw(true);
  });
 function GetResultHandler(data) { // overiding ajax request js function
   if (data.hasOwnProperty('errors')) {
     oops();
   } else if (data.hasOwnProperty('success') && data.success == "19199212") {
     Object.keys(data.data).forEach(function(key) {
       $('input[name="id"]').val(data.data["id"]);
       $('input[name="question"]').val(data.data["question"]);
       $('input[name="start_date"]').val(data.data["starting_date"]);
       $('input[name="end_date"]').val(data.data["end_date"]);


     });

     $.each( $('input[name="answers[]"]'), function (key) {
       $(this).val(data.data["answers"][key].answer)
     });
     $.each( $('input:radio[name="correct_answer"]'), function (key) {
       if($(this).data('value') ==data.data["answer_id"]){
         $(this).prop('checked', true);
       }else{
         $(this).prop('checked', false);
       }
     });
       $('#addModal').modal('show');
}
}

function toggleStatus(ele) {
  if(confirm("Are you sure ?")){
    var id = $(ele).data("id");
    var status = $(ele).data("status");
    var xhttp;
    if (window.XMLHttpRequest) {
      xhttp = new XMLHttpRequest();
    } else {
      // code for IE6, IE5
      xhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xhttp.addEventListener("load", function() {

      data = JSON.parse(this.responseText);
      if (data.hasOwnProperty('errors')) {
        oops();
      } else if (data.hasOwnProperty('success') && data.success == "19199212") {
        window.location.reload();
   }


    });
    xhttp.addEventListener("error", function() {
      oops();
    });

      xhttp.open("POST", baseUrl +"/"+id+"/"+status, false);
  xhttp.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
    xhttp.send();
  }
}
  </script>
@endsection
