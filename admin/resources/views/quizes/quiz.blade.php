<div class="modal fade" id="addModal" >
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center">Quiz</h4>
              </div>
              <div class="modal-body">
                <form class="form-horizontal" id="quizeForm" enctype="multipart/form-data">
                  <input type="hidden" name="id" form="quizeForm">
              <div class="box-body">
                <div class="form-group">
                  <label for="modalName" class="control-label">Question</label>

                    <input type="text" class="form-control" name="question"  placeholder="Question" form="quizeForm">

                </div>

                  <div class="form-group">
                    <label for="modalAdults" class="control-label">Answer 1</label>
                    <div class="input-group">
                      <input type="text" class="form-control" name="answers[]" form="quizeForm">
                    <div class="input-group-addon">
                      <input type="radio" name="correct_answer" form="quizeForm" data-value="1">
                    </div>
                  </div>
                  </div>

                <div class="form-group">
                  <label for="modalAdults" class="control-label">Answer 2</label>
                  <div class="input-group">
                  <input type="text" class="form-control" name="answers[]" form="quizeForm">
                  <div class="input-group-addon">
                    <input type="radio" name="correct_answer" form="quizeForm" data-value="2">
                  </div>
                </div>
                </div>
                <div class="form-group">
                  <label for="modalAdults" class="control-label">Answer 3</label>
                  <div class="input-group">
                  <input type="text" class="form-control" name="answers[]" form="quizeForm">
                  <div class="input-group-addon">
                    <input type="radio" name="correct_answer" form="quizeForm" data-value="3">
                  </div>
                </div>
                </div>
                <div class="form-group">
                  <label for="modalAdults" class="control-label">Answer 4</label>
                  <div class="input-group">
                  <input type="text" class="form-control" name="answers[]" form="quizeForm">
                  <div class="input-group-addon">
                    <input type="radio" name="correct_answer" form="quizeForm" data-value="4">
                  </div>
                </div>
                </div>


                <div class="form-group">
                  <label for="modalName" class="control-label">Start Date</label>

                  <input type="date" class="form-control" name="start_date"  placeholder="Start Date" form="quizeForm">

                </div>

                <div class="form-group">
                  <label for="modalName" class="control-label">End Date</label>

                  <input type="date" class="form-control" name="end_date"  placeholder="End Date" form="quizeForm">

                </div>


              </div>
              <div class="row" id="quizeFormErrors">

          </div>
            </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" form="noForm" class="btn btn-primary FormSubmit" data-form="quizeForm">Save</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
