@extends('includes.app')
@section('title', 'Quizes')
@section('aditionalCss')
  <style>
    table.dataTable tbody td {
      word-break: break-word;
      vertical-align: top;
    }
  </style>

  @endsection
@section('content')
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Quizes
        <small>All the quizes</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
    <div class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <div class="box-tools pull-right" style="position: inherit;">

                  <button type="button" onclick="showForm()" class="btn bg-navy ">Add New</button>
          </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
        <table class="table dataTable table-bordered table-hover table-responsive" width="100%">
          <thead>
            <tr>
              <th>Question</th>
              <th>Start Date</th>
              <th>End Date</th>
              <th class="text-center">Status</th>
              <th class="text-center">Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($quizes as $quize)
              <tr>
                <td>{{$quize->question}} </td>
                <td>{{$quize->starting_date}}</td>
                <td width="30%">{{$quize->end_date}}

                  </td>
                <td class="text-center">
                  @if ($quize->status == 1)
                    <small class="label  bg-green">Active</small>
                  @elseif ($quize->status ==  0)
                    <small class="label  bg-yellow">Inactive</small>
                  @endif
                </td>
                <td class="text-center">
                  <button type="button"  onclick="getData({{$quize->id}})"  class="btn  btn-info btn-xs ">Edit/ View</button>

                  @if ($quize->status == 1)
                    <button type="button" onclick="toggleStatus(this)" data-status="0"  data-id="{{$quize->id}}"  class="btn  btn-warning btn-xs ">Inactivate</button>
                  @elseif ($quize->status ==  0)
                    <button type="button"  onclick="toggleStatus(this)" data-status="1"  data-id="{{$quize->id}}"  class="btn  btn-success btn-xs ">Activate</button>
                  @endif
                  <button type="button"  onclick="toggleStatus(this)" data-status="-1"  data-id="{{$quize->id}}"  class="btn  btn-danger btn-xs ">Delete</button>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
</div>
</div>
      </div>
    </div>
    </div>
    @include('quizes.quiz')
@endsection

@section('aditionalJs')
  <script type="text/javascript">
  // start use for request js
    baseUrl =  "{{url("quizes")}}";
      url =  baseUrl;
      primaryKey = "id"; // use to set update url
      reloadAfterSubmit = true;
      // end use for request js

      function showForm() {
        resetForm("quizeForm");
        $('#addModal').modal('show')
      }
      function submitData(formId, type) {
        var formData = new FormData(document.querySelector("#" + formId))
        let correctAnswer = $('#addModal').find('input[name="correct_answer"]:checked').data('value');
        formData.set("correct_answer", correctAnswer ? correctAnswer : "")
        submitForm(formData);
      }
 $('.table').DataTable({
   'paging'      : true,
   'lengthChange': false,
   'searching'   : false,
   'ordering'    : true,
   'info'        : true,
   'autoWidth'   : false,

 });

 function GetResultHandler(data) { // overiding ajax request js function
   if (data.hasOwnProperty('errors')) {
     oops();
   } else if (data.hasOwnProperty('success') && data.success == "19199212") {
     Object.keys(data.data).forEach(function(key) {
       $('input[name="id"]').val(data.data["id"]);
       $('input[name="question"]').val(data.data["question"]);
       $('input[name="start_date"]').val(data.data["starting_date"]);
       $('input[name="end_date"]').val(data.data["end_date"]);


     });

     $.each( $('input[name="answers[]"]'), function (key) {
       $(this).val(data.data["answers"][key].answer)
     });
     $.each( $('input:radio[name="correct_answer"]'), function (key) {
       if($(this).data('value') ==data.data["answer_id"]){
         $(this).prop('checked', true);
       }else{
         $(this).prop('checked', false);
       }
     });
       $('#addModal').modal('show');
}
}

function toggleStatus(ele) {
  if(confirm("Are you sure ?")){
    var id = $(ele).data("id");
    var status = $(ele).data("status");
    var xhttp;
    if (window.XMLHttpRequest) {
      xhttp = new XMLHttpRequest();
    } else {
      // code for IE6, IE5
      xhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xhttp.addEventListener("load", function() {

      data = JSON.parse(this.responseText);
      if (data.hasOwnProperty('errors')) {
        oops();
      } else if (data.hasOwnProperty('success') && data.success == "19199212") {
        window.location.reload();
   }


    });
    xhttp.addEventListener("error", function() {
      oops();
    });

      xhttp.open("POST", baseUrl +"/"+id+"/"+status, false);
  xhttp.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
    xhttp.send();
  }
}
  </script>
@endsection
