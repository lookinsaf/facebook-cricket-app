<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    protected $table = 'quizes';
   protected $fillable = ['question', 'status', 'starting_date', 'end_date', 'answer_id'];

   public function answers()
   {
       return $this->hasMany(Answer::class, 'question_id', 'id');
   }

    function answer(){
        return $this->belongsTo(Answer::class, 'answer_id');
    }
}
