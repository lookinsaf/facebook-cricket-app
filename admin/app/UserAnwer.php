<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAnwer extends Model
{
    protected $table = "user_answers";

    function question(){
        return $this->belongsTo(Quiz::class, 'question_id');
    }
    function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    function answer(){
        return $this->belongsTo(Answer::class, 'answer_id');
    }
}
