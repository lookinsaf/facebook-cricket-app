<?php

namespace App\Http\Controllers;

use App\Quiz;
use App\Answer;
use Illuminate\Http\Request;
use Validator;
use Response;

class QuizController  extends Controller
{
    public function viewAll()
    {

          $quizes = Quiz::where("status", '!=', -1)->get();
      return view("quizes.quizes", ["quizes" => $quizes]);
    }

    public function store(Request $request)
    {
      $validator = $this->validateInputs($request->all());

      if ($validator->fails()) {
        return Response::json(['errors' => $validator->getMessageBag()->toArray()], 200);
      }else{
        $quiz = Quiz::create([
          "question" => $request->question,
          "starting_date" => $request->start_date,
          "end_date" => $request->end_date,
            "answer_id" => $request->correct_answer
        ]);


        if($quiz){
            foreach ($request->answers as $answer){
                Answer::create([
                    'question_id' => $quiz->id,
                'answer' => $answer
            ]);
        }
          return Response::json(['success' => "19199212", "message" => "Quiz added successfully"], 200);
        }
      }
    }
public function getQuiz($id)
{
    $quiz = Quiz::with('answers')->find($id);
  return Response::json(['success' => "19199212", "data" => $quiz], 200);
}

public function setQuiz($id, Request $request)
{
  $validator = $this->validateInputs($request->all(), $id);

  if ($validator->fails()) {
    return Response::json(['errors' => $validator->getMessageBag()->toArray()], 200);
  }else{
    $quiz = Quiz::with('answers')->find($id);

      $quiz->question = $request->question;
      $quiz->answer_id = $request->correct_answer;
      $quiz->starting_date = $request->start_date;
      $quiz->end_date = $request->end_date;

      foreach ($quiz->answers as $key => $answer){
          $answer->update([
              "answer" => $request->answers[$key]
          ]);
      }

      if ($quiz->save()) {
        return Response::json(['success' => "19199212", "message" => "Question updated successfully"], 200);
      }
    }

}
public function toggleStatus($id, $status)
{
    $quiz = Quiz::where('status', "!=", -1)->find($id);
  if ($quiz) {
      $quiz->status = $status;
    if ($quiz->save()) {
      return Response::json(['success' => "19199212", "message" => "Quiz updated successfully"], 200);
    }
  }
}
    public function validateInputs($inputs, $id = -1)
    {
      $rules = [
        "question" => "required",
        "answers" => "required|array|min:4",
        "correct_answer" => "required",
        "start_date" => "required|date",
        "end_date" => "required|date|after_or_equal:start_date"
      ];
$msgs = [
    "correct_answer.required" => "Please select the correct answer."
];
      return Validator::make($inputs, $rules, $msgs);
    }
}
