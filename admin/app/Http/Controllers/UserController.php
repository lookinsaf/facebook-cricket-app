<?php

namespace App\Http\Controllers;

use Validator;
use Response;
use Auth;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function login(Request $request)
    {
      $validator = Validator::make($request->all(), $this->getUserValidations());

      if($validator->fails()){
        return Response::json(['errors' => $validator->getMessageBag()->toArray()], 200);
      }else{
        $adminUserdata = array(
                 'username' => $request->user_name,
                 'password' => $request->password,
                 'user_level' => "admin",
             );
        $agentUserdata = array(
                 'username' => $request->user_name,
                 'password' => $request->password,
                 'user_level' => "Agent",
             );
             if(Auth::attempt($adminUserdata) || Auth::attempt($agentUserdata)){
               return Response::json(['success' => "19199212", "message" => "Loged in successfully."], 200);
             }else{
               return Response::json(['errors' => ["error" => "Sorry, we coudn't find your details"]], 200);
             }
      }
    }

    public function getUserValidations()
    {
      $rules = array(
         'user_name' => 'required',
            'password' => 'required'
        );
return $rules;
    }
}
