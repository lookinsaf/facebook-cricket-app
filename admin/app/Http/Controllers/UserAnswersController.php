<?php

namespace App\Http\Controllers;

use App\UserAnwer;
use Illuminate\Http\Request;
use Datatables;
class UserAnswersController extends Controller
{
    function viewAll(){

        return view('user-answers.user-answers');
    }

    function getAnswersData(Request $request){
//        $start_date = (!empty($request->start_date)) ? $request->start_date :  '';
//        $end_date = (!empty($request->end_date)) ? ($request->end_date) : ('');

        $answers = UserAnwer::whereHas('question')
            // ->where('answer_id', '!=', null)
            ->with('question.answer', 'user', 'answer');

        if($request->start_date != ""){
            $answers->where('created_at',">=", $request->start_date." 00:00:00");
        }
        if($request->end_date != ""){
            $answers->where('created_at',"<=", $request->end_date." 23:59:59");
        }
        $answers = $answers->get();
        $return = collect();
        foreach($answers->groupBy('game_id') as $gameId => $gameAnswers){
            $game = collect();
            $gameAnswerCount = $gameAnswers->count();
            $gameCorrectAnswers = 0;
            $gameScore = 0;
            if($gameAnswerCount > 0){
                $reportData = array();
                if($gameAnswers->get(0)->user){
                    $reportData['user_name'] = $gameAnswers->get(0)->user->name;
                    $reportData['email'] = $gameAnswers->get(0)->user->email;
                }else{
                    $reportData['user_name'] = "";
                    $reportData['email'] ="";
                }

                $reportData['date'] = $gameAnswers->get(0)->created_at->format('d M Y  H:i:s');
                foreach ($gameAnswers as $gameAnswer){
                    if($gameAnswer->answer_id == $gameAnswer->question->answer_id){
                        $gameCorrectAnswers++;
                    }
                }

            }

            if($gameAnswerCount != 0){
                $gameScore = ($gameCorrectAnswers / $gameAnswerCount) * 100;
            }

            $reportData['score'] = $gameScore;
            $return->push(collect($reportData));
        }
      return  Datatables::of($return)
            ->make(true);
    }
}
