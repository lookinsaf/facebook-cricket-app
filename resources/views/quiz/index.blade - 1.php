@extends('includes.app')
@section('title', 'Welcome')

@section('aditionalCss')

    <link href="{{url('css/main.css')}}"rel="stylesheet"type="text/css"/>
    <link href="{{url('css/grid.css')}}"rel="stylesheet"type="text/css"/>
    <link href="{{url('css/progress-wizard.min.css')}}"rel="stylesheet"type="text/css"/>
@endsection
@section('content')

    <div class="bg">
        <div id="topbar" align="right"> <img src="{{url('resource/male-circle.png')}}" class="acnt_img" alt="account info">
            <span class="user_name">Hello {{auth()->user()->name}}</span>
            <a href="{{url('logout')}}" class="btn btn-info btn-sm">Logout</a>
        </div>
        <div class="spacer"></div><br>

        <div class="container col-12 col-s-9s"><br><br>
            <ul class="progress-indicator">
                @foreach($quizes as $key => $quiz)
                    <li id="li{{$key+1}}"> <span class="bubble"></span> Step {{$key+1}}</li>
                @endforeach

                <li id="li{{count($quizes)+1}}"> <span class="bubble"></span> Step {{count($quizes)+1}}</li>
            </ul>
        </div>

        <div id="navContent">
            <div class="row" align="center">
                <div id="game1"></div>
                <div id="game2"></div>
            </div>
            <div class="row" id="timer-row">
                <div id="countdown" align="center">
{{--                    <!-- <img src="resource/timer-clock-nt.png" class="clock_img" alt="timer clock"> -->--}}
                </div>
            </div>

        </div>

    </div>


@endsection
@section('aditionalJs')
    <script src="{{url('js/jquery.js')}}"></script>
    <script src="{{url('js/jquery.min.js')}}"></script>
    <script src="{{url('js/jquery.countdown360.js')}}"></script>
    <script src="{{url('js/controller.js')}}"></script>

    <script>
        var questionNumber=0;
        var questionBank=new Array();
        var stage="#game1";
        var stage2=new Object;
        var questionLock=false;
        var numberOfQuestions;
        var score=0;
        var quizlist = {!! json_encode($quizes->shuffle()) !!};
        var _token = "{{csrf_token()}}";


        $.each(quizlist, function(key, data) {
            for(i=0;i<quizlist.length;i++){
                questionBank[i]=new Array;
                questionBank[i][0]=data.question;
                questionBank[i][1]=data.answers[0].answer;
                questionBank[i][2]=data.answers[1].answer;
                questionBank[i][3]=data.answers[2].answer;
                questionBank[i][4]=data.answers[3].answer;
                questionBank[i][5]=data.answer_id;
                questionBank[i][6]=data.id;
            }
            numberOfQuestions=questionBank.length;
            // displayFinalSlide()

        })//gtjson
        displayQuestion();
    </script>
@endsection

