<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body, html {
  height: 100%;
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

/* Shared */
.loginBtn {
  box-sizing: border-box;
  position: relative;
  /* width: 13em;  - apply for fixed size */
  margin: 0.2em;
  padding: 2px 15px 2px 46px;
  border: none;
  text-align: left;
  line-height: 34px;
  white-space: nowrap;
  border-radius: 0.2em;
  font-size: 16px;
  color: #FFF;
}
.loginBtn:before {
  content: "";
  box-sizing: border-box;
  position: absolute;
  top: 0;
  left: 0;
  width: 34px;
  height: 100%;
}
.loginBtn:focus {
  outline: none;
}
.loginBtn:active {
  box-shadow: inset 0 0 0 32px rgba(0,0,0,0.1);
}


/* Facebook */
.loginBtn--facebook {
  background-color: #4C69BA;
  background-image: linear-gradient(#4C69BA, #3B55A0);
  /*font-family: "Helvetica neue", Helvetica Neue, Helvetica, Arial, sans-serif;*/
  text-shadow: 0 -1px 0 #354C8C;
}
.loginBtn--facebook:before {
  border-right: #364e92 1px solid;
  background: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/14082/icon_facebook.png') 6px 6px no-repeat;
}
.loginBtn--facebook:hover,
.loginBtn--facebook:focus {
  background-color: #5B7BD5;
  background-image: linear-gradient(#5B7BD5, #4864B1);
}

.hero-image {
  /*background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url("../../../public/resource/ground.jpg");*/
  background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url("{{url('resource/background.jpg')}}");
  height: 100%;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;
}

.hero-text {
  text-align: center;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  color: white;
}

/*.hero-text button {
  border: none;
  outline: 0;
  display: inline-block;
  padding: 10px 25px;
  color: black;
  background-color: #63ce00;
  text-align: center;
  cursor: pointer;
  font-weight: bold;
}*/

/*.hero-text button:hover {
  background-color: blue;
  color: white;
}*/

#yfm {
    position:fixed;
    right:0;
    bottom:0;
    margin:1%;
    width:12%;
    height:45%;
    background:url('{{url('resource/yfm.png')}}') no-repeat bottom right;
    background-size:100%;
}

#center_img  img {
    display:block;
    margin:auto;
    width: 60%;
}
button.playBtn {
    width: 100%;
    height: 50px;
    border-radius: 4px;
    background-color: #5cb85c;
    color: #fff;
    font-size: 20px;
    font-weight: 500;
    border: none;
}
button.playBtn:hover {
    background-color: #448844;
    color: #fff;
    cursor: pointer;
}
</style>
</head>
<body> 

<div class="hero-image">
  <div class="hero-text">
    <h1>Y Cricket Trivia</h1>
    <div id="center_img">
      <img src="{{url('resource/logo.png')}}" class="blink-image" />
    </div>
    <!-- <h1 style="font-size:50px">Facebook Cricket Quiz</h1> -->
    <p></p>
    <form></form>
    
    <a href="{{url('/quiz')}}"> 
    <button class="playBtn" style="text-align:center;">
   Play
    </button>
    </a>
    <!-- <a href="{{url('/redirect')}}" class="loginBtn loginBtn--facebook"><button>Login with Facebook</button></a> -->
  </div>
  <div id="yfm"></div>
</div>


<!-- <a href="{{url('/redirect')}}" class="btn btn-primary">Login with Facebook</a> -->

<!-- <link href="{{url('css/blink.css')}}"rel="stylesheet"type="text/css"/> -->
</body>
</html>
