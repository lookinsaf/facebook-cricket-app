<html>
    <head>
      <meta name="csrf-token" content="{{ csrf_token() }}" />
      <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>@yield('title') | Facebook Cricket Quiz</title>
        <!-- Tell the browser to be responsive to screen width -->
 <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
 @include('includes.basicCss')
 @yield('aditionalCss')
</head>
<body >
            @yield('content')
   @include('includes.basicJs')
   @yield('aditionalJs')
    </body>
</html>
