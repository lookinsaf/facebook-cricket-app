<header>
			<div class="container_12">
				<div class="grid_12">
					<div class="menu_block">
						<nav class="horizontal-nav full-width horizontalNav-notprocessed">
							<ul class="sf-menu">
								<li @if (\Request::is("/")) class="current"
		            @endif><a href="{{url("/")}}">HOME</a></li>

								<li @if (\Request::is("tours")) class="current"
		            @endif><a href="{{url("tours")}}">TOURS</a></li>

								<li @if (\Request::is("register")) class="current"
		            @endif><a href="{{url("register")}}">REGISTER</a></li>

								<li><a @if (\Request::is("login")) class="current"
		            @endif href="{{url("../admin/public/login")}}">LOGIN</a></li>

								<li><a @if (\Request::is("contact-us")) class="current"
		            @endif  href="{{url("contact-us")}}">CONTACTS</a></li>
							</ul>
						</nav>
						<div class="clear"></div>
					</div>
				</div>
				<div class="grid_12">
					<h1>
						<a href="{{url("/")}}">
							<img src="images/logo.png" alt="Your Happy Family">
						</a>
					</h1>
				</div>
			</div>
		</header>
