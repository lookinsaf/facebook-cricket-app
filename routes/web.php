<?php
Route::get('/redirect', 'SocialAuthFacebookController@redirect');
Route::get('/callback', 'SocialAuthFacebookController@callback');

Route::get('/play', 'SocialAuthFacebookController@play')->name('play');
Route::get('/', 'SocialAuthFacebookController@index')->name('login');
Route::get('/logout', 'SocialAuthFacebookController@logout')->name('logout');

Route::group(['middleware' => 'auth'], function(){
    Route::get('/quiz', 'QuizController@index');
    Route::post('/quiz/answer', 'QuizController@updateUserAnswers');
});

