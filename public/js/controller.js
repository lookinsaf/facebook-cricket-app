function displayQuestion(){

 var questionNow = questionNumber+1;

 $("#li"+questionNumber).removeClass("active");
 $("#li"+questionNow).removeClass("completed");
 $("#li"+questionNumber).addClass("completed");
 $("#li"+questionNow).addClass("active");

 $("#incorrect").hide();
 $("#correct").hide();
  $(".pyro").hide();


	var q1=questionBank[questionNumber][1];
	var q2=questionBank[questionNumber][2];
	var q3=questionBank[questionNumber][3];
	var q4=questionBank[questionNumber][4];

	var countdown =  $("#countdown").countdown360({
       	 	radius      : 60,
         	seconds     : 15,
         	fontColor   : '#FFFFFF',
         	autostart   : false,
         	onComplete  : function () { if(questionNumber<numberOfQuestions){changeQuestion(); }}
		   });
			countdown.start();
			// console.log('countdown360 ',countdown);
		 	$(document).on("click","button",function(e){
		 		e.preventDefault();
		 	});
			$(stage).append('<div class="row questionPane"><div class="cus-col-12 col-s-9s" style="padding:0px;"><h3>'
				+questionBank[questionNumber][0]+
				'</h3></div></div><br><div class="cus-col-12 col-s-12 menu"><ul><div class="row"><div class="cus-col-12 col-s-12" style="padding-top: 0px; padding-bottom: 0px;"><li id="1" class="cus-col-4 col-s-6"><label id="label_1" class="strokeme2"><input type="checkbox" id="1" class="option">&nbsp'
				+q1+
				'</label></li><li id="2" class="cus-col-4 col-s-6"><label id="label_2" class="strokeme2"><input type="checkbox" id="2" class="option">&nbsp'
				+q2+
				'</label></li></div><div class="cus-col-12 col-s-12"  style="padding-top: 0px; padding-bottom: 0px;"><li id="3" class="cus-col-4 col-s-6"><label id="label_3" class="strokeme2"><input type="checkbox" id="3" class="option">&nbsp'
				+q3+
				'</label></li><li id="4" class="cus-col-4 col-s-6"><label id="label_4" class="strokeme2"><input type="checkbox" id="4" class="option">&nbsp'
				+q4+
				'</label></li></ul></div></div></div>');

	$("#game2").css( 'pointer-events', 'none' );
	$(stage).css( 'pointer-events', 'all' );

 $('.option').click(function(){
	 updateUserAnswer(questionBank[questionNumber][6], this.id);
  if(questionLock==false){questionLock=true;
  if(questionBank[questionNumber][5] == this.id){
  
  $("#countdown").hide();
  $("#correct").show();
   /*$(stage).append('<div class="feedback1 strokeme">CORRECT</div>');*/

   score++;
   }else{
   
   $("#countdown").hide();
   $("#label_"+questionBank[questionNumber][5]).addClass("blink");
   $("#incorrect").show();

   /*$(stage).append('<div class="feedback2 strokeme">INCORRECT</div>');*/

  }
	  setInterval(blink_text, 500);
  setTimeout(function(){
	  changeQuestion();
  },5000);
 }});


}//display question

	
	
	function updateUserAnswer(question, answer){
		$.ajax({
			url: "quiz/answer",
			type : "post",
			data : {
				"_token" : _token,
				"game_id" : gameId,
				"question_id" : question,
				"answer_id" : answer
			}
		});
	}
	
	
	function changeQuestion(){
		
		questionNumber++;
	
	if(stage=="#game1"){
		stage2="#game1";stage="#game2";

	}
		else{
			stage2="#game2";stage="#game1";
		}

	$("#incorrect").hide();
 	$("#correct").hide();
	$("#countdown").show();
	
	if(questionNumber<numberOfQuestions){displayQuestion();}else{displayFinalSlide();}
	
	 $(stage2).animate({"right": "+=1200px"},"slow", function() {$(stage2).css('right','-1200px');$(stage2).empty();});
	 $(stage).animate({"right": "+=1200px"},"slow", function() {questionLock=false;});
		$(stage2).css( 'pointer-events', 'none' );
		$(stage).css( 'pointer-events', 'all' );
	}//change question
	



	function displayFinalSlide(){

		var marks = (score/numberOfQuestions)*100;
		var questionNow = questionNumber+1;

		$("#countdown").hide();

		$("#li"+questionNumber).removeClass("active");
		$("#li"+questionNumber).addClass("completed");
		$("#li"+questionNow).addClass("completed");
		/*$(stage).append('<div class="row questionPane">' +
			'<div class="col-12 col-s-9s" style="padding:0px;">' +
			'<h2>Thank You For Joining With Us !</h2>' +
			'</div><div class="col-12 col-s-9s" style="padding:0px;">' +
			'<h3>Your Score :</h3>' +
			'</div></div><br><br><img src="resource/ring.png" class="ring_img" alt="ring" style="width:100px;"><br><h1 style="margin-top:-72px;">'+marks+'</h1>' +
			'<br><br><a href="quiz" class="btn btn-warning">Retry</a>'+
			'</div>');
		$("#countdown").hide();*/

		$(stage).append('<div class="row questionPane"><div class="cus-col-12 col-s-9s" style="padding:0px;"><h3 class="glow">Your Score is Calculating !</h3></div><div class="cus-col-12 col-s-9s" style="padding:0px;"><h4 class="glow strokeme2">Loading ...</h4></div></div><br><br><label class="strokeme"></label><br><br><img src="resource/cric.gif" class="ring_img" alt="ring" style="width:100px;"></div>');

		setTimeout(function(){

			$(stage).empty();
			$(stage).append('<div class="row questionPane"><div class="cus-col-12 col-s-9s" style="padding:0px;"><h3 class="glow">Thank You For Joining With Us !</h3></div><div class="cus-col-12 col-s-9s" style="padding:0px;"><h4 class="glow strokeme2">Your Score :</h4></div></div><br><br><label class="strokeme">'+username+'</label><br><br><img src="resource/ring.png" class="ring_img" alt="ring" style="width:100px;"><br><h2 class="glow" style="margin-top:-72px; color:brown;">'+marks+'</h2><br><br><a href="quiz" class="btn btn-warning">Retry</a></div>');
		
			if (marks==100) {
			$(".pyro").show();
			$(stage).empty();
			$(stage).append('<div class="row questionPane"><div class="cus-col-12 col-s-9s" style="padding:0px;"><h3 class="glow blink-image">Congratulations !!!<br>You\'re The Winner </h3></div><div class="cus-col-12 col-s-9s" style="padding:0px;"><h4 class="glow strokeme2">Your Score :</h4></div></div><br><br><label class="strokeme">'+username+'</label><br><br><img src="resource/ring.png" class="ring_img" alt="ring" style="width:100px;"><br><h2 class="glow" style="margin-top:-72px; color:brown;">'+marks+'</h2><br><br><a href="quiz" class="btn btn-warning">Retry</a></div>');
			}

		},5000);	

	}//display final slide

function blink_text() {
	$('.blink').fadeOut(500);
	$('.blink').fadeIn(500);
}
	
	
