<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAnwer extends Model
{
    protected $table = "user_answers";
    protected $fillable = ['user_id', 'question_id', 'answer_id', 'game_id'];
}
