<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = ['answer', 'question_id'];

    function question(){
        $this->belongsTo(Quiz::class, 'question_id');
    }
}
