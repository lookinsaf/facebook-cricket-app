<?php

namespace App\Http\Controllers;

use App\Quiz;
use App\UserAnwer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Uuid;

class QuizController extends Controller
{
    function  index(){
//         auth()->loginUsingId(1, true);
         $uuid = Uuid::generate(4)->string;
//        $answerdQuizes = auth()->user()->answers->pluck('question_id')->toArray();

       $quizes = Quiz::with('answers')
            ->whereStatus(1)
            ->whereDate('starting_date', '<=', Carbon::now())
            ->whereDate('end_date', '>=', Carbon::now())
            ->inRandomOrder()
//            ->whereNotIn('id', $answerdQuizes)
            ->limit(5)
            ->get();

        foreach ($quizes as $quize){
            UserAnwer::create([
                'user_id' => auth()->user()->id,
                'question_id' => $quize->id,
                'game_id' =>  $uuid
            ]);
        }

        return view('quiz.index', [
            "quizes" => $quizes,
            "game_id" => $uuid
        ]);
    }

    function updateUserAnswers(Request $request){
        if($request->has('question_id') && $request->has('answer_id')){
            UserAnwer::where('user_id', auth()->user()->id)
            ->where('game_id', $request->game_id)
                ->where('question_id', $request->question_id)
            ->update([
                'answer_id' => $request->answer_id
            ]);
        }
    }
}
